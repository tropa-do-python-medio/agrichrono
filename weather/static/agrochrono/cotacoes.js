document.addEventListener("DOMContentLoaded", function() {
    var linksToRemove = document.querySelectorAll('a[href*="noticiasagricolas.com.br"]');
    linksToRemove.forEach(function(link) {
        link.parentNode.removeChild(link);
    });
});

var lastOpenWidget = null;

function toggleWidget(widgetId) {
    var widgetOverlay = document.getElementById(widgetId);
    var widgetContent = widgetOverlay.querySelector('.widget-content');
    var overlayText = widgetOverlay.querySelector('.overlay-text');
    
    if (lastOpenWidget && lastOpenWidget !== widgetOverlay) {
        lastOpenWidget.querySelector('.widget-content').style.display = 'none';
        lastOpenWidget.querySelector('.overlay-text').style.display = 'block';
    }

    if (widgetContent.style.display === 'block') {
        widgetContent.style.display = 'none';
        overlayText.style.display = 'block';
        lastOpenWidget = null;
    } else {
        widgetContent.style.display = 'block';
        overlayText.style.display = 'none';
        lastOpenWidget = widgetOverlay;
    }
}

function searchWidgets(event) {
    if (event) {
        event.preventDefault();
    }
    
    const query = document.getElementById('searchInput').value.toLowerCase();
    const widgets = document.getElementsByClassName('widget-container');
    let found = false;
    
    for (let widget of widgets) {
        const overlayText = widget.querySelector('.overlay-text').textContent.toLowerCase();
        if (overlayText.includes(query)) {
            widget.style.display = 'block';
            found = true;
        } else {
            widget.style.display = 'none';
        }
    }
    
    if (!found) {
        document.getElementById('noResults').style.display = 'block';
    } else {
        document.getElementById('noResults').style.display = 'none';
    }
    return false;
}
