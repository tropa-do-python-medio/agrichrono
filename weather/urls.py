from django.urls import path
from . import views

app_name = 'agrochrono'

urlpatterns = [
    path('', views.index, name='index'),
    path('cotacoes/', views.CotacoesView.as_view(), name='cotacoes'),
    path('maps/', views.maps, name='maps'),
    path('save_location/', views.save_location, name='save_location'),
    path('delete_location/<int:location_id>/', views.delete_location, name='delete_location'),
    path('register/', views.register, name='registrar'),
    path('login/', views.user_login, name='login'),
    path('noticias/', views.NoticiasView.as_view(), name='noticias')
]