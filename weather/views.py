import requests
import geocoder
import json
from django.views import generic
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponse
from .models import SavedLocation
from django.contrib.auth import authenticate, login as auth_login
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from .forms import UserRegistrationForm, LoginForm
from django.template.loader import render_to_string

def get_current_location_coordinates():
    g = geocoder.ip('me')
    if g.latlng is not None:
        return g.latlng
    else:
        return None

def get_current_weather_hgbrasil(lat, lon):
    url = f'https://api.hgbrasil.com/weather?key=426ebee6&lat={lat}&lon={lon}&fields=temp,date,time,condition_code,description,currently,humidity,cloudiness,rain,wind_speedy,wind_direction,wind_cardinal,moon_phase,condition_slug,city_name,timezone'
    response = requests.get(url)
    
    if response.status_code == 200:
        data = response.json().get('results', {})
        
        moon_phase_mapping = {
            "new": "Nova",
            "waxing_crescent": "Crescente",
            "first_quarter": "Quarto Crescente",
            "waxing_gibbous": "Crescente Gibosa",
            "full": "Cheia",
            "waning_gibbous": "Minguante Gibosa",
            "last_quarter": "Quarto Minguante",
            "waning_crescent": "Minguante"
        }
        
        weather_info = {
            'temperature': data.get('temp', 'N/A'),
            'date': data.get('date', 'N/A'),
            'time': data.get('time', 'N/A'),
            'description': data.get('description', 'N/A'),
            'humidity': data.get('humidity', 'N/A'),
            'cloudiness': data.get('cloudiness', 'N/A'),
            'rain': data.get('rain', 'N/A'),
            'wind_speedy': data.get('wind_speedy', 'N/A'),
            'wind_direction': data.get('wind_direction', 'N/A'),
            'wind_cardinal': data.get('wind_cardinal', 'N/A'),
            'moon_phase': moon_phase_mapping.get(data.get('moon_phase', ''), data.get('moon_phase', 'N/A')),
            'city_name': data.get('city', 'N/A')
        }
        
        return weather_info
    else:
        print("Erro ao acessar a API hgbrasil:", response.status_code)
        return None

def five_days_prevision(lat, lon):
    url = f'https://api.openweathermap.org/data/2.5/forecast?lat={lat}&lon={lon}&units=metric&lang=pt_br&appid=af561dcbdd69011cb3d872ad2dffcbe7'
    response = requests.get(url)
    prevision_data = []
    if response.status_code == 200:
        data = response.json()
        for forecast in data['list']:
            dt_txt = forecast['dt_txt']
            temp = forecast['main']['temp']
            description = forecast['weather'][0]['description']
            humidity = forecast['main']['humidity']
            precipitation = round(forecast['pop'] * 100, 1)
            rain_3hours = forecast.get('rain', {}).get('3h', 0)
            snow_3hours = forecast.get('snow', {}).get('3h', 0)

            date, time = dt_txt.split(' ')
            formatted_date = "/".join(reversed(date.split('-')))
            formatted_time = time[:5]

            prevision_data.append({
                "date": formatted_date,
                "time": formatted_time,
                "temperature": temp,
                "humidity": humidity,
                "description": description,
                "precipitation": precipitation,
                "rain": rain_3hours,
                "snow": snow_3hours
            })
    else:
        print("Erro ao acessar a API:", response.status_code)
    return prevision_data

def maps(request):
    coordinates = get_current_location_coordinates()
    return render(request, 'maps/maps.html', {'coordinates': coordinates})

@csrf_exempt
@login_required
def save_location(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        lat = data.get('lat')
        lon = data.get('lon')
        name = data.get('name')

        if lat is not None and lon is not None and name:
            SavedLocation.objects.create(user=request.user, latitude=lat, longitude=lon, name=name)
            return JsonResponse({'status': 'success'})
        else:
            return JsonResponse({'status': 'error'}, status=400)
    return JsonResponse({'status': 'error'}, status=405)

@csrf_exempt
@login_required
def delete_location(request, location_id):
    location = get_object_or_404(SavedLocation, pk=location_id)
    location.delete()
    return JsonResponse({'message': 'Localização excluída com sucesso.'})

def weather_alerts(lat, lon):
    api_key = 'ec66f849b3ad4d7f860171005242005'
    base_url = 'http://api.weatherapi.com/v1'
    endpoint = '/forecast.json'

    location = f'{lat}, {lon}'

    params = {
        'key': api_key,
        'q': location,
        'alerts': 'yes',
        'lang': 'pt'
    }
    response = requests.get(base_url + endpoint, params=params)
    if response.status_code == 200:
        data = response.json()
        alerts_data = data.get('alerts', {}).get('alert', []) 
        
        formatted_alerts = []
        for alert_data in alerts_data:
            formatted_alert = {
                'headline': alert_data.get('headline', ''),
                'msgType': alert_data.get('msgtype', ''),
                'severity': alert_data.get('severity', ''),
                'urgency': alert_data.get('urgency', ''),
                'areas': alert_data.get('areas', ''),
                'category': alert_data.get('category', ''),
                'certainty': alert_data.get('certainty', ''),
                'event': alert_data.get('event', ''),
                'note': alert_data.get('note', ''),
                'effective': alert_data.get('effective', ''),
                'expires': alert_data.get('expires', ''),
                'desc': alert_data.get('desc', ''),
                'instruction': alert_data.get('instruction', '')
            }
            formatted_alerts.append(formatted_alert)
        return formatted_alerts
    else:
        print("Erro ao acessar a API:", response.status_code)
        return []

def extract_information(desc):
    keywords = ['WHAT', 'WHERE', 'WHEN', 'IMPACTS']
    extracted_info = []

    for keyword in keywords:
        index = desc.find(keyword)
        if index != -1:
            next_keyword_index = desc.find('*', index + len(keyword))
            if next_keyword_index != -1:
                extracted_info.append(desc[index:next_keyword_index])
            else:
                extracted_info.append(desc[index:])

    return extracted_info

def index(request):
    lat = request.GET.get('lat')
    lon = request.GET.get('lon')
    if lat and lon:
        lat = lat.replace(',', '.')
        lon = lon.replace(',', '.')
        lat = float(lat)
        lon = float(lon)
    else:
        coordinates = get_current_location_coordinates()
        if coordinates:
            lat, lon = coordinates
        else:
            lat, lon = None, None

    weather_info = None
    prevision = None
    alerts = None
    available_dates = []
    selected_date = request.GET.get('date', None)
    saved_locations = []

    if request.user.is_authenticated:
        saved_locations = SavedLocation.objects.filter(user=request.user)

    selected_location = request.GET.get('location_id')
    if selected_location == "current_location":
        lat, lon = get_current_location_coordinates()
        if lat is None or lon is None:
            error_message = "Não foi possível obter a localização atual. Por favor, tente novamente mais tarde."

    elif selected_location:
        location = SavedLocation.objects.get(id=selected_location)
        lat, lon = location.latitude, location.longitude    

    if lat and lon:
        weather_info = get_current_weather_hgbrasil(lat, lon)
        prevision = five_days_prevision(lat, lon)
        alerts = weather_alerts(lat, lon)

        for forecast in prevision:
            date_str = forecast["date"]
            if date_str not in available_dates:
                available_dates.append(date_str)

        if selected_date:
            selected_prevision = [forecast for forecast in prevision if forecast["date"].startswith(selected_date)]
            prevision = selected_prevision

    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        print(prevision, selected_date)
        html = render_to_string('previsao_parcial.html', {
            'prevision': prevision,
            'selected_date': selected_date
        })
        return HttpResponse(html)

    if alerts:
        for alert in alerts:
            desc = alert['desc']
            extracted_info = extract_information(desc)
            alert['extracted_info'] = '\n'.join(extracted_info)

    return render(request, 'index.html', {
        'weather_info': weather_info,
        'prevision': prevision,
        'alerts': alerts,
        'available_dates': available_dates,
        'selected_date': selected_date,
        'saved_locations': saved_locations,
        'lat': lat,
        'lon': lon
    })


class CotacoesView(generic.TemplateView):
    template_name = 'cotacoes/cotacoes.html'

class NoticiasView(generic.TemplateView):
    template_name = 'noticias/noticias.html'

@csrf_exempt
def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            auth_login(request, user)
            return redirect('/')
    else:
        form = UserRegistrationForm()
    return render(request, 'user/register.html', {'form': form})

@csrf_exempt
def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return redirect('/')
            else:
                return render(request, 'user/login.html', {'form': form, 'erro': 'Username or Password is invalid.'})
    else:
        form = LoginForm()
    return render(request, 'user/login.html', {'form': form})
