import requests

# Insira sua chave de API do Alpha Vantage aqui
API_KEY = 'DYKQ4LLNROS4D1LT'

# Função para extrair a cotação do trigo
def get_wheat_price():
    url = f'https://www.alphavantage.co/query?function=WHEAT&interval=monthly&apikey={API_KEY}'
    response = requests.get(url)
    data = response.json()
    print(data)  # Adicionando este print para verificar os dados retornados
    if 'data' in data:
        # Obtendo o preço mais recente de trigo
        latest_data = data['data'][0]
        wheat_price = latest_data['value']
        return wheat_price
    return None

# Exemplo de uso
wheat_price = get_wheat_price()
if wheat_price:
    print(f'O preço do trigo é: {wheat_price}')
else:
    print('Não foi possível obter o preço do trigo.')